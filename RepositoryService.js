/**
 * kermit - soa infrastructure for node js
 *
 * @package     kermit-mongoose
 * @copyright   Copyright (c) 2016, Alrik Zachert
 * @license     https://gitlab.com/kermit-js/kermit-mongoose/blob/master/LICENSE BSD-2-Clause
 */

'use strict';

const
  Service = require('kermit/Service');

/**
 * The abstract kermit mongoose repository class.
 */
class RepositoryService extends Service {
  /**
   * Return the default service config.
   *
   * @returns {Object}
   */
  getDefaultServiceConfig() {
    return {
      connectionServiceKey: 'mongoose'
    }
  }

  /**
   * @param {kermit.ServiceManager} serviceManager
   */
  constructor(serviceManager) {
    super(serviceManager);

    this.connectionService = null;
    this.model = null;
    this.schema = null;
  }

  /**
   * Fetch the connection service.
   *
   * @returns {RepositoryService}
   */
  bootstrap() {
    this.connectionService = this.getServiceManager().get(
      this.serviceConfig.get('connectionServiceKey'),
      true
    );

    return this;
  }

  /**
   * Return the instance of the connection service.
   *
   * @returns {ConnectionService}
   */
  getConnectionService() {
    return this.connectionService;
  }

  /**
   * Return the mongooose singleton.
   *
   * @returns {mongoose}
   */
  getMongoose() {
    return this.connectionService.getMongoose();
  }

  /**
   * Fetch the schema of the model managed by this repository.
   *
   * @returns {mongoose.Schema}
   */
  getSchema() {
    if (!this.schema) {
      let schema = new (this.getMongoose().Schema)(
        this.getSchemaDefinition(),
        this.getSchemaOptions()
      );

      this.applySchemaPlugins(schema);
      this.schema = schema;
    }

    return this.schema;
  }

  /**
   * The actual schema definition of the model.
   *
   * @returns {Obejct}
   */
  getSchemaDefinition() {
    return {};
  }

  /**
   * Return optional schema options.
   *
   * @returns {Object}
   */
  getSchemaOptions() {
    return {};
  }

  /**
   * Register plugins on the schema.
   *
   * @param {mongoose.Schema}
   * @returns {RepositoryService}
   */
  applySchemaPlugins(schema) {
    return this;
  }

  /**
   * Return the model name by either returning the configured one or calculte it
   * from the repository class name.
   *
   * @returns {String}
   */
  getModelName() {
    return this.serviceConfig.get(
      'modelName',
      this.constructor.name.replace('Repository', '')
    );
  }

  /**
   * Fetch and return the mpdel managed by the repository.
   *
   * @returns {mongoose.Model}
   */
  getModel() {
    if (!this.model) {
      this.model = this.connectionService.getConnection().model(
        this.getModelName(),
        this.getSchema()
      );
    }

    return this.model;
  }
}

module.exports = RepositoryService;
